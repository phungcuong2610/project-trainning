FROM node:18-alpine

RUN mkdir /project-trainning

WORKDIR /project-trainning

COPY package*.json yarn*.lock ./

RUN yarn install --frozen-lockfile

RUN yarn install --force

COPY . .

RUN yarn

RUN yarn build 

CMD [ "yarn", "start:dev" ]
