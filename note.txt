- Pipelines được tạo ra từ các stages, trong các stages chứa các jobs
- stages xác định những việc mình muốn làm ví dụ như build, test, deploy
- các jobs được nhóm trong các giai đoạn của stages như việc chạy script, ...

- Tóm tắt quy trình chạy của pipelines:
 + Pipelines sẽ tiến hành chạy từ đầu file đến cuối nên cái nào khai báo trước sẽ chạy trước
 + Đầu tiên khi chương trình chạy sẽ được chia làm 3 stage:
    build -> test -> deploy
 + Trong các stage bao gồm các job:
    build:  build-job
    test:   1. test-job1
            2. test-job2
    deploy: deploy-prod

- Vậy tại sao phải dùng với docker để CI-CD??? Vì chỉ cần file gitlab nó đã tự động có thể chạy các job và hoàn thành các bước mình cần r mà
Đúng là vậy nhưng phải sử dụng docker để tiến hành CD còn nếu viết kia thì chỉ là CI.
 + Docker in Docker: DinD: cung cấp 1 máy ảo docker (tạo ra 1 docker) trong này dùng để chạy các service container trong docker compose
(ở đây dùng để tử khởi động lại trên server) -> Phải dùng đến Docker

- Bắt đầu với CI-CD nào ...
 + Đầu tiên sẽ tạo 1 Dockerfile để tạo 1 service với image và container ứng dụng của mình (ở đây sử dụng nodejs)
 + Tiếp theo sẽ viết docker compose bao gồm các service mình cài đặt và cần sử dụng ...
 // + docker tag project-trainning-be:latest registry.gitlab.com/phungcuong2610/project-trainning (docker tag <image>:<tag> registry.gitlab.com/<user-repo>/<reponame>) (Không cần dùng câu lệnh này ...)
 + docker build -t registry.gitlab.com/phungcuong2610/project-trainning . (docker build -t registry.gitlab.com/<user-repo>/<name-repo> .)
 + docker push registry.gitlab.com/phungcuong2610/project-trainning (docker push <tag-name>)
 + Sau khi build xong chúng ta đã tự tạo ra 1 image mới rồi lúc này sửa docker-compose như trong project
 + Bây giờ cứ mỗi lần đẩy code mà phải cập nhật lại cái này thì hơi đần -> áp dụng CI để CD cho nó ...
 + Sửa file .gitlab-ci.yml nào ...
 -> Đến đây là coi như setup xong CI ở local
 -> Tiếp theo sẽ đưa lên server nào -> Đăng kí 1 cái AWS EC2 hoặc Azure Machine...
 + Sau khi remote đến server thì bắt đầu tiền hành setup
 + Setup Docker trên server:
 + Project này mở 2 port là 3030 của service và 5432 của db -> Nên azure mở ... 
 + Test...